import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, MinLengthValidator, Validators } from '@angular/forms';

@Component({
  selector: 'app-tarjeta-credito',
  templateUrl: './tarjeta-credito.component.html',
  styleUrls: ['./tarjeta-credito.component.css']
})
export class TarjetaCreditoComponent implements OnInit {

  listTarjetas: any[] = [
    {titular: 'Jhon Phillip', numeroTarjeta: '134645633', fechaExpiracion: '12/26', cvv: '123'},
    {titular: 'Mia Link', numeroTarjeta: '352414145', fechaExpiracion: '04/24', cvv: '321'}
  ];

  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      titular:['', Validators.required],
      numeroTarjeta: ['', [Validators.required,Validators.maxLength(12), Validators.minLength(12)]],//Validators.pattern(/^[0-9,$]*$/)
      fechaExpiracion: ['', [Validators.required, Validators.maxLength(5), Validators.minLength(5),,Validators.pattern(/^[0-9,/]*$/)]],
      cvv: ['', [Validators.required, Validators.maxLength(4), Validators.maxLength(4),,Validators.pattern(/^[0-9,/]*$/)]]
    })
  }

  ngOnInit(): void {
  }

  agregarTarjeta(){
    
    const tarjeta: any = {
      titular: this.form.get('titular')?.value,
      numeroTarjeta: this.form.get('numeroTarjeta')?.value,
      fechaExpiracion: this.form.get('fechaExpiracion')?.value,
      cvv: this.form.get('cvv')?.value
    }
    this.listTarjetas.push(tarjeta);
    this.form.reset();
  }

  eliminarTarjeta(index: number){
    this.listTarjetas.splice(index, 1);
  }

}
